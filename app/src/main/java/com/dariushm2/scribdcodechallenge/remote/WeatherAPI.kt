package com.dariushm2.scribdcodechallenge.remote

import com.dariushm2.scribdcodechallenge.model.ForecastResponse
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherAPI {

    companion object RemoteApi {
        const val appId = "0aee4dbe94bd7370e887a57b14b5c806"
        val retrofit = Retrofit.Builder()
            .baseUrl("https://api.openweathermap.org/data/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(WeatherAPI::class.java)
    }

    @GET("2.5/forecast")
    fun get5DaysForcast(@Query("id") cityCode: Long,
                        @Query("appid") appId: String = RemoteApi.appId): Call<ForecastResponse>
}