package com.dariushm2.scribdcodechallenge.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.dariushm2.scribdcodechallenge.R
import com.dariushm2.scribdcodechallenge.model.ForecastResponse
import com.dariushm2.scribdcodechallenge.model.MainInfo
import kotlinx.android.synthetic.main.forecast_item.view.*
import java.text.SimpleDateFormat
import java.util.*


class ForecastListAdapter(
    private val activity: MainActivity,
    private var forecast: ForecastResponse
) : RecyclerView.Adapter<ForecastListAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.forecast_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val mainInfo: MainInfo = forecast.forecasts[position].mainInfo

        Calendar.getInstance()

        holder.itemView.txtDate.text =
            activity.getString(R.string.date, getDate(forecast.forecasts[position].date))
        holder.itemView.txtFeelsLike.text =
            activity.getString(R.string.feelsLike, mainInfo.feelsLike)
        holder.itemView.txtMinTemp.text = activity.getString(R.string.minTemp, mainInfo.minTemp)
        holder.itemView.txtMaxTemp.text = activity.getString(R.string.maxTemp, mainInfo.maxTemp)
    }

    override fun getItemCount(): Int = forecast.forecasts.size

    fun getDate(timestamp: Long): String {
        val calendar = Calendar.getInstance(Locale.ENGLISH)
        calendar.timeInMillis = timestamp * 1000L
        val formatter = SimpleDateFormat("dd-MM-yyyy hh:mm:ss", Locale.CANADA)
        return formatter.format(calendar.time).toString()
    }


    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        init {
            val txtDate: TextView = view.findViewById(R.id.txtDate)
            val txtFeelsLike: TextView = view.findViewById(R.id.txtFeelsLike)
            val txtMinTemp: TextView = view.findViewById(R.id.txtMinTemp)
            val txtMaxTemp: TextView = view.findViewById(R.id.txtMaxTemp)
        }

    }

}
