package com.dariushm2.scribdcodechallenge.view


import android.os.Bundle
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.dariushm2.scribdcodechallenge.R
import com.dariushm2.scribdcodechallenge.model.City
import com.dariushm2.scribdcodechallenge.model.ForecastResponse
import kotlinx.android.synthetic.main.activity_main.*


const val TAG = "Scribd"


class MainActivity : AppCompatActivity(), ForecastContract {

    private val forecastContract: ForecastContract = this

    private lateinit var presenter: ForecastPresenter

    private lateinit var adapter: ArrayAdapter<City>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        presenter = ForecastPresenter.getInstance(this, forecastContract)

        if (savedInstanceState != null) {
            if (presenter.cities != null)
                setCitiesAdapter(presenter.cities!!)
            if (presenter.forecast != null)
                setForecastsAdapter(presenter.forecast!!)
        }

        txtCity.setOnItemClickListener { parent, view, position, id ->
            Log.e(TAG, "${adapter.getItem(position)?.name} -- ${adapter.getItem(position)?.id}")
            presenter.onCitySelected(adapter.getItem(position)!!)
            progressBar.show()
            txtCity.clearFocus()
        }

    }


    override fun onCitiesLoaded(cities: Array<City>) {
        setCitiesAdapter(cities)
    }

    private fun setCitiesAdapter(cities: Array<City>) {
        adapter = ArrayAdapter(applicationContext, android.R.layout.simple_list_item_1, cities)
        txtCity.setAdapter(adapter)

    }

    override fun onForecastFailed(message: String) {
        Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show()
        progressBar.hide()
    }

    override fun onForecastReceived(forecast: ForecastResponse) {
        setForecastsAdapter(forecast)
    }

    private fun setForecastsAdapter(forecast: ForecastResponse) {
        val adapter = ForecastListAdapter(this, forecast)

        rvForcast.layoutManager = LinearLayoutManager(applicationContext)
        rvForcast.adapter = adapter
        Log.e(TAG, "${forecast.forecasts.get(0).date}")
        Log.e(TAG, "onReceived")
        progressBar.hide()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (isFinishing)
            presenter.onDestroy()
    }
}
