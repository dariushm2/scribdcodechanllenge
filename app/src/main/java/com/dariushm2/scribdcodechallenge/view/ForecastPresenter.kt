package com.dariushm2.scribdcodechallenge.view


import android.util.Log
import com.dariushm2.scribdcodechallenge.model.City
import com.dariushm2.scribdcodechallenge.model.ForecastResponse
import com.dariushm2.scribdcodechallenge.remote.WeatherAPI
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.concurrent.Executors

class ForecastPresenter private constructor(
    val activity: MainActivity,
    val forecastContract: ForecastContract
) {

    companion object {
        private var instance : ForecastPresenter? = null

        fun  getInstance(activity: MainActivity, forecastContract: ForecastContract): ForecastPresenter {
            if (instance == null)  // NOT thread safe!
                instance = ForecastPresenter(activity, forecastContract)
            return instance!!
        }
    }
    var cities: Array<City>? = null
    var forecast: ForecastResponse? = null


    init {
        readCities()
    }


    private fun readCities() {
        val executor = Executors.newSingleThreadExecutor()
        executor.execute {
            val bufferedReader =
                BufferedReader(InputStreamReader(activity.assets.open("city.list.min.json")))
            val json = bufferedReader.readLine()
            val gson = Gson()
            cities = gson.fromJson(json, Array<City>::class.java)

            activity.runOnUiThread {
                forecastContract.onCitiesLoaded(cities!!)
            }
        }
    }

    fun onCitySelected(city: City) {
        Log.e(TAG, "onCitySelected")
        WeatherAPI.retrofit.get5DaysForcast(city.id).enqueue(object : Callback<ForecastResponse> {

            override fun onFailure(call: Call<ForecastResponse>, t: Throwable) {
                forecastContract.onForecastFailed(t.message!!)
            }

            override fun onResponse(call: Call<ForecastResponse>, response: Response<ForecastResponse>) {
                if (response.body() != null) {
                    if (response.isSuccessful) {
                        forecast = response.body() as ForecastResponse
                        forecastContract.onForecastReceived(response.body() as ForecastResponse)
                    } else
                        forecastContract.onForecastFailed(response.message())
                } else
                    forecastContract.onForecastFailed("Forecast Failed!")
            }
        })
    }

    fun onDestroy() {
        instance = null
    }

}