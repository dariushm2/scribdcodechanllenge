package com.dariushm2.scribdcodechallenge.view

import com.dariushm2.scribdcodechallenge.model.City
import com.dariushm2.scribdcodechallenge.model.ForecastResponse

interface ForecastContract {


    fun onCitiesLoaded(cities: Array<City>)

    fun onForecastReceived(forecast: ForecastResponse)

    fun onForecastFailed(message: String)


}