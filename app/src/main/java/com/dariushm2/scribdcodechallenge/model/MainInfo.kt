package com.dariushm2.scribdcodechallenge.model

import com.google.gson.annotations.SerializedName

data class MainInfo(@SerializedName("feels_like") val feelsLike: Float,
                    @SerializedName("temp_min") val minTemp: Float,
                    @SerializedName("temp_max") val maxTemp: Float)