package com.dariushm2.scribdcodechallenge.model

import com.google.gson.annotations.SerializedName


data class ForecastResponse(@SerializedName("cod") val code: String,
                            //@SerializedName("message") val message: String,
                            @SerializedName("list") val forecasts: List<Forecast>)
