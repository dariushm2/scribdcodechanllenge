package com.dariushm2.scribdcodechallenge.model

import com.google.gson.annotations.SerializedName

data class City(@SerializedName("id") val id: Long,
                @SerializedName("name") val name: String,
                @SerializedName("country") val country: String) {

    override fun toString(): String {
        return "$name, $country"
    }
}