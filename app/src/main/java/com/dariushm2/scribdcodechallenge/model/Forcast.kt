package com.dariushm2.scribdcodechallenge.model

import com.google.gson.annotations.SerializedName

data class Forecast(@SerializedName("dt") val date: Long,
                    @SerializedName("main") val mainInfo: MainInfo)
